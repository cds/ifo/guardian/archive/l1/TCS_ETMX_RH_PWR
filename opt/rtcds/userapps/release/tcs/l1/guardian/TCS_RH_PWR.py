from guardian import GuardState
import time
import tcsconfig

__, OPTIC , __ , __ = SYSTEM.split('_')
## FUNCTIONS

def adj_rh_power(initial_power , diff):
    ezca['SETUPPERPOWER'] = initial_power + diff
    ezca['SETLOWERPOWER'] = initial_power + diff

## insert error checker

 ## STATES ##

class INIT(GuardState):
    index = 0
    request = False
    def main(self):
        return True

    def run(self):
        return 'UNFILTERED_RH_INPUT'

class DISABLE_INVERSION_RESET(GuardState):
    index = 11
    request = False
    def main(self):
        ezca.switch('INVERSE_FILTER', 'OUT','FM5', 'OFF')                 # turn off the RH filter output
        ezca['INVERSE_FILTER_RSET'] = 2                                   # clear filter history
#        ezca['INVERSE_FILTER_MASK'] = 0                                   # Disable the inverse inverse path from lho alog 46363  # Get rid of mask (will remove from model) currently set manually


class UNFILTERED_RH_INPUT(GuardState):
    index = 20
    def main(self):
        ezca.switch('INVERSE_INVERSE_FILTER', 'OUT', 'OFF')              # Ensure nothing goes on this path
        log('Disabled Inverse Filter')
        ezca.switch('INVERSE_FILTER', 'FM5', 'OFF')                 # turn off the RH filter output
        ezca.switch('INVERSE_FILTER', 'FM4', 'OFF')                 # turn off the RH filter output
        ezca.switch('INVERSE_FILTER', 'FM3', 'OFF')                 # turn off the RH filter output
        ezca.switch('INVERSE_FILTER', 'FM2', 'OFF')                 # turn off the RH filter output
        ezca.switch('INVERSE_FILTER', 'FM1', 'OFF')                 # turn off the RH filter output
        log('Change ring heater power with L1:TCS-OPTIC_RH_SETUPPERPOWER and L1:TCS-OPTIC_RH_SETLOWERPOWER')

    def run(self):
        notify('Adjust RH with SETUPPERPOWER and SETLOWERPOWER') 

        return True

class PREPARE_FILTER(GuardState):    
    index = 25
    request = False
    def main(self):
        log('Copy ring heater value to inverse filters and reset filters')
        
        #VB20240509 This below commented section was giving sdf issues when guardian goes down from power loss. re-written to draw a value to tscconfig
        #currentValue = ezca['SETUPPERPOWER']
        currentValue = tcsconfig.starting_priorval[OPTIC]
        
        ezca['PRIORVAL'] = currentValue
        ezca['INVERSE_FILTER_OFFSET'] = currentValue
        ezca.switch('INVERSE_FILTER', 'OFFSET', 'ON') 
        ezca['INVERSE_FILTER_RSET'] = 2                                   # clear filter history
        log('Inverse Filter Ready')
        return


class FILTER_RH_INPUT_FASTEST(GuardState):
    index = 29
    def main(self):
    # Set up filtering in previous state
        ezca.switch('INVERSE_FILTER', 'FM4', 'ON') 
        ezca.switch('INVERSE_FILTER', 'FM1', 'FM2', 'FM3', 'FM5', 'OFF')
        log('Enabled Fastest Inverse Filter')
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET
                
    def run(self):
        if (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) < 0:
            log('RH attempting to go to negative power')
            adj_rh_power(0,0)
        elif (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) > 10:
            log('RH attempting to go above 10W')
            adj_rh_power(0,10)
        else:
            adj_rh_power(0, ezca['INVERSE_FILTER_OUTVAL'])  # reads filter output the RH power 
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET

        time.sleep(1)
        return True

class FILTER_RH_INPUT_SELFHEATING(GuardState):
    index = 31
    def main(self):
    # Set up filtering in previous state
       ezca.switch('INVERSE_FILTER', 'FM3', 'ON') 
       ezca.switch('INVERSE_FILTER', 'FM1', 'FM2', 'FM4', 'FM5', 'OFF')
       log('Enabled Inverse Filter to match self heating substrate lens')
       notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET
                
    def run(self):
        if (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) < 0:
            log('RH attempting to go to negative power')
            adj_rh_power(0,0)
        elif (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) > 10:
            log('RH attempting to go above 10W')
            adj_rh_power(0,10)
        else:
            adj_rh_power(0, ezca['INVERSE_FILTER_OUTVAL'])   # reads filter output the RH power
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET

        time.sleep(1)
        return True

class FILTER_RH_INPUT_NOOVERSHOOT(GuardState):
    index = 33
    def main(self):
    # Set up filtering in previous state
        ezca.switch('INVERSE_FILTER', 'FM5', 'ON') 
        ezca.switch('INVERSE_FILTER', 'FM1', 'FM2', 'FM3', 'FM4', 'OFF')
        log('Enabled Inverse Filter with no overshoot in ring heater power')
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET
                
    def run(self):
        if (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) < 0:
            log('RH attempting to go to negative power')
            adj_rh_power(0,0)
        elif (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) > 10:
            log('RH attempting to go above 10W')
            adj_rh_power(0,10)
        else:
            adj_rh_power(0, ezca['INVERSE_FILTER_OUTVAL'])   # reads filter output the RH power
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET

        time.sleep(1)
        return True

class FILTER_RH_INPUT_SURFACE(GuardState):
    index = 35
    def main(self):
    # Set up filtering in previous state
        ezca.switch('INVERSE_FILTER', 'FM1', 'FM3', 'FM4', 'FM5', 'OFF')
        ezca.switch('INVERSE_FILTER', 'FM2', 'ON') 
        log('Enabled Inverse Filter to match the surface transient time constant')
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET
                
    def run(self):
        if (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) < 0:
            log('RH attempting to go to negative power')
            adj_rh_power(0,0)
        elif (ezca['INVERSE_FILTER_OUTPUT'] + ezca['PRIORVAL']) > 10:
            log('RH attempting to go above 10W')
            adj_rh_power(0,10)
        else:
            adj_rh_power(0, ezca['INVERSE_FILTER_OUTVAL'])   # reads filter output the RH power
        notify('Adjust the RH power with GRD SET') # L1:TCS-OPTIC_RH_INVERSE_FILTER_OFFSET

        time.sleep(1)
        return True


class OFFLOAD_FILTER_RESET(GuardState):     # Essentially the same as the UNFILTERED_RH_INPUT state for the moment
    index = 39
    request = False
    def main(self):
        log('Offload inverse filter offset to priorval and reset filter')
        ezca['PRIORVAL'] = ezca['INVERSE_FILTER_OFFSET']
        ezca['INVERSE_FILTER_OFFSET'] = 0
        ezca['INVERSE_FILTER_RSET'] = 2                                   # clear filter history
        return

class FREEZE_TRANSIENT(GuardState):     # Essentially the same as the UNFILTERED_RH_INPUT state for the moment
    index = 40
    request = False
    def main(self):
        log('Applied ring heater power frozen, history still accumulating')
        return


edges = [('INIT', 'UNFILTERED_RH_INPUT'), 
         ('UNFILTERED_RH_INPUT', 'INIT'),
         ('UNFILTERED_RH_INPUT', 'PREPARE_FILTER'),
         ('PREPARE_FILTER','FILTER_RH_INPUT_FASTEST'),
         ('FILTER_RH_INPUT_FASTEST', 'FILTER_RH_INPUT_SELFHEATING'),
         ('FILTER_RH_INPUT_FASTEST', 'FILTER_RH_INPUT_NOOVERSHOOT'),
         ('FILTER_RH_INPUT_FASTEST', 'FILTER_RH_INPUT_SURFACE'),
         ('FILTER_RH_INPUT_FASTEST', 'OFFLOAD_FILTER_RESET'),
         ('PREPARE_FILTER', 'FILTER_RH_INPUT_SELFHEATING'),
         ('FILTER_RH_INPUT_SELFHEATING', 'FILTER_RH_INPUT_FASTEST'),
         ('FILTER_RH_INPUT_SELFHEATING', 'FILTER_RH_INPUT_NOOVERSHOOT'),
         ('FILTER_RH_INPUT_SELFHEATING', 'FILTER_RH_INPUT_SURFACE'),
         ('FILTER_RH_INPUT_SELFHEATING', 'OFFLOAD_FILTER_RESET'),
         ('PREPARE_FILTER', 'FILTER_RH_INPUT_NOOVERSHOOT'),
         ('FILTER_RH_INPUT_NOOVERSHOOT', 'FILTER_RH_INPUT_FASTEST'),
         ('FILTER_RH_INPUT_NOOVERSHOOT', 'FILTER_RH_INPUT_SELFHEATING'),
         ('FILTER_RH_INPUT_NOOVERSHOOT', 'FILTER_RH_INPUT_SURFACE'),
         ('FILTER_RH_INPUT_NOOVERSHOOT', 'OFFLOAD_FILTER_RESET'),
         ('PREPARE_FILTER', 'FILTER_RH_INPUT_SURFACE'),
         ('FILTER_RH_INPUT_SURFACE', 'FILTER_RH_INPUT_SELFHEATING'),
         ('FILTER_RH_INPUT_SURFACE', 'FILTER_RH_INPUT_NOOVERSHOOT'),
         ('FILTER_RH_INPUT_SURFACE', 'FILTER_RH_INPUT_FASTEST'),
         ('FILTER_RH_INPUT_SURFACE', 'OFFLOAD_FILTER_RESET'),
#         ('INIT','DISABLE_INVERSION_RESET'),
#         ('DISABLE_INVERSION_RESET','INIT'),
         ('OFFLOAD_FILTER_RESET', 'UNFILTERED_RH_INPUT')]
